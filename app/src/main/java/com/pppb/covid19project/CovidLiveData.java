package com.pppb.covid19project;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;

import com.pppb.covid19project.api.CoronavirusCovid19API;
import com.pppb.covid19project.model.DayOne;
import com.pppb.covid19project.model.Summary;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CovidLiveData extends ViewModel {
    private static final String TAG = "debug";
    private MutableLiveData<Summary> summaryMutableLiveData;
    private MutableLiveData<List<DayOne>> dayOneList;
    private MutableLiveData<String> currentCountry = new MutableLiveData<>();

    public void setCurrentCountry(String currentCountry) {
        this.currentCountry.setValue(currentCountry);
    }

    public LiveData<Summary> getSummary() {
        if (summaryMutableLiveData == null) {
            summaryMutableLiveData = new MutableLiveData<Summary>();
            loadSummary();
        }

        return summaryMutableLiveData;
    }

    public LiveData<List<DayOne>> getNewDayOne() {
        if (dayOneList == null) {
            dayOneList = new MutableLiveData<>();
            loadDayOne(currentCountry.getValue());
        }
        return dayOneList;
    }

    public void loadDayOne(String str) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(CoronavirusCovid19API.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        CoronavirusCovid19API api = retrofit.create(CoronavirusCovid19API.class);
        Call<List<DayOne>> call = api.getDayOne(str);
        List<DayOne> list = new ArrayList<>();

        call.enqueue(new Callback<List<DayOne>>() {
            @Override
            public void onResponse(Call<List<DayOne>> call, Response<List<DayOne>> response) {
                Log.d("Code", "onRespDayOne : " + response.code());
                dayOneList.setValue(response.body());
            }

            @Override
            public void onFailure(Call<List<DayOne>> call, Throwable t) {
                Log.d("Failure", "onFailure: " + call.toString());
            }
        });
    }


    private void loadSummary() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(CoronavirusCovid19API.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        CoronavirusCovid19API api = retrofit.create(CoronavirusCovid19API.class);
        Call<Summary> call = api.getSummary();

        call.enqueue(new Callback<Summary>() {
            @Override
            public void onResponse(Call<Summary> call, Response<Summary> response) {
                Log.d("Code", "onResponse: " + response.code());
                summaryMutableLiveData.setValue(response.body());
            }

            @Override
            public void onFailure(Call<Summary> call, Throwable t) {
                Log.d("Failure", "onFailure: " + call.toString());
            }
        });
    }
}
