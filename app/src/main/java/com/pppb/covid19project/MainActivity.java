package com.pppb.covid19project;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkCapabilities;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ExpandableListAdapter;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.pppb.covid19project.databinding.ActivityMainBinding;
import com.pppb.covid19project.model.ExpandableListDataPump;
import com.pppb.covid19project.view.AlertDialog;
import com.pppb.covid19project.view.CustomExpandableListAdapter;
import com.pppb.covid19project.view.DataFragment;
import com.pppb.covid19project.view.FAQFragment;
import com.pppb.covid19project.view.FragmentListener;
import com.pppb.covid19project.view.HomeFragment;
import com.pppb.covid19project.view.IOnBackPressed;
import com.pppb.covid19project.view.SettingsFragment;
import com.pppb.covid19project.view.WebViewFragment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MainActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener, FragmentListener {
    private ActivityMainBinding binding;
    private HomeFragment homeFragment;
    private DataFragment dataFragment;
    private SettingsFragment settingsFragment;
    private FragmentManager fragmentManager;
    private FAQFragment faqFragment;
    private WebViewFragment webViewFragment;
    private SharedPreferences sharedPreferences;
    public static final String KEY_THEME = "NIGHT_MODE";

    // Data for CustomExpandableListView
    List<String> expandableListTitle;
    HashMap<String, List<String>> expandableListDetail;
    ExpandableListAdapter expandableListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.binding = ActivityMainBinding.inflate(this.getLayoutInflater());
        setContentView(binding.getRoot());
        this.fragmentManager = this.getSupportFragmentManager();
        this.sharedPreferences = getPreferences(MODE_PRIVATE);

        boolean isNightMode = this.loadTheme();
        if(isNightMode){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        } else {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }

        ConnectivityManager connectivityManager = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkCapabilities networkCapabilities = connectivityManager.getNetworkCapabilities(connectivityManager.getActiveNetwork());

        if (networkCapabilities == null){
            openNoInetDialog();
        }

        // Create CustomExpandableListAdapter
        ExpandableListDataPump eldp = new ExpandableListDataPump();
        eldp.generateData();
        expandableListDetail = eldp.getDataChild();
        expandableListTitle = new ArrayList<String>(eldp.getDataHeader());
        expandableListAdapter = new CustomExpandableListAdapter(this, expandableListTitle, expandableListDetail);

        this.homeFragment = new HomeFragment();
        this.dataFragment = new DataFragment();
        this.faqFragment = FAQFragment.newInstance(expandableListAdapter);
        this.settingsFragment = SettingsFragment.newInstance(sharedPreferences);
        this.webViewFragment = new WebViewFragment();

        loadFragment(this.homeFragment);
        BottomNavigationView bottomNavigationView = binding.bottomNavBar;
        bottomNavigationView.setOnNavigationItemSelectedListener(this);
    }

    private void openNoInetDialog() {
        AlertDialog alertDialog = new AlertDialog();
        alertDialog.show(getSupportFragmentManager(), "alert dialog");
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Fragment fragment = null;

        switch (item.getItemId()){
            case R.id.home:
                fragment = this.homeFragment;
                break;
            case R.id.data:
                fragment = this.dataFragment;
                break;
            case R.id.setting:
                fragment = this.settingsFragment;
                break;
            case R.id.faq:
                fragment = this.faqFragment;
                break;
        }

        return loadFragment(fragment);
    }

    public boolean loadFragment(Fragment fragment){
        if (fragment != null){
            fragmentManager.beginTransaction().replace(R.id.fragment_container, fragment).commit();
            return true;
        }
        return false;
    }

    public boolean loadTheme() {
        boolean res = sharedPreferences.getBoolean(KEY_THEME, false);
        return res;
    }

    @Override
    public void changePage(String page) {
        if(page.equals("webview")) {
            loadFragment(this.webViewFragment);
        } else if(page.equals("home")) {
            loadFragment(this.homeFragment);
        }
    }

    @Override
    public void onBackPressed() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        if(!(fragment instanceof IOnBackPressed) || !((IOnBackPressed) fragment).onBackPressed()) {
            super.onBackPressed();
        }
    }

    @Override
    public void restartApp(){
        Intent i = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(i);
        finish();
    }
}