package com.pppb.covid19project.api;

import com.pppb.covid19project.model.DayOne;
import com.pppb.covid19project.model.Summary;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface CoronavirusCovid19API {

    String BASE_URL = "https://api.covid19api.com/";

    @GET("summary")
    Call<Summary> getSummary();

    @GET("dayone/country/{country}")
    Call<List<DayOne>> getDayOne(@Path("country") String country);
}
