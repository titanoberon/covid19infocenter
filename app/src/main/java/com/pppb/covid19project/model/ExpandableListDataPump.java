package com.pppb.covid19project.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ExpandableListDataPump {

    private List<String> listDataHeader;
    private HashMap<String, List<String>> listDataChild;
    private List<ArrayList<String>> listDataChildTemp;

    public void generateData() {
        this.listDataHeader = new ArrayList<String>();
        this.listDataChild = new HashMap<String, List<String>>();

        this.listDataHeader.add("Apa itu Novel Coronavirus (2019-nCoV)?");//1
        this.listDataHeader.add("Bagaimana cara mencegah penularan COVID-19?");//2
        this.listDataHeader.add("Apa saja gejala COVID-19?");//3
        this.listDataHeader.add("Seberapa bahaya COVID-19 ini?");//4
        this.listDataHeader.add("Bagaimana manusia bisa terinfeksi COVID-19?");//5
        this.listDataHeader.add("Bisakah manusia terinfeksi novel coronavirus dari hewan?");//6
        this.listDataHeader.add("Benarkah COVID-19 berasal dari kelelawar atau hewan lainnya?");//7
        this.listDataHeader.add("Apakah COVID-19 dapat ditularkan antar manusia?"); //8
        this.listDataHeader.add("Berapa lama virus ini bertahan di permukaan benda?"); //9
        this.listDataHeader.add("Apakah sudah ada vaksin atau pengobatan spesifik untuk COVID-19?"); //10
        this.listDataHeader.add("Apakah antibiotik efektif dalam mencegah dan mengobati COVID-19?"); //11
        this.listDataHeader.add("Siapa saja yang berisiko terinfeksi COVID-19?");//12
        this.listDataHeader.add("Manakah yang lebih rentan terinfeksi COVID-19, apakah orang yang lebih tua, atau orang yang lebih muda?");//13
        this.listDataHeader.add("Bagaimana membedakan antara COVID-19 dengan influenza biasa?");//14
        this.listDataHeader.add("Berapa lama waktu yang diperlukan sejak tertular/terinfeksi hingga muncul gejala COVID-19?");//15
//        this.listDataHeader.add("");

        listDataChildTemp = new ArrayList<ArrayList<String>>();

        //1
        listDataChildTemp.add(new ArrayList<String>() {{
            add(
                    "Novel coronavirus (2019-nCoV) adalah jenis baru coronavirus yang belum pernah diidentifikasi sebelumnya pada manusia. Coronavirus merupakan keluarga besar virus yang menyebabkan penyakit pada manusia dan hewan. Pada manusia menyebabkan penyakit mulai flu biasa hingga penyakit yang serius seperti Middle East Respiratory Syndrome (MERS) dan Sindrom Pernapasan Akut Berat/ Severe Acute Respiratory Syndrome (SARS)." +
                            "\n\nPada 11 Februari 2020, World Health Organization (WHO) mengumumkan nama penyakit yang disebabkan 2019-nCov, yaitu Coronavirus Disease (COVID-19)."
            );
        }});

        //2
        listDataChildTemp.add(new ArrayList<String>() {{
            add(
                    "Hingga saat ini, belum ada vaksin untuk mencegah penularan COVID-19." +
                            "\nSalah satu cara yang bisa dilakukan untuk mencegah tertularnya virus ini adalah:" +

                            "\n\n1. Menjaga kesehatan dan kebugaran agar sistem imunitas/ kekebalan tubuh meningkat." +
                            "\n\n2. Mencuci tangan menggunakan air dan sabun atau  hand-rub berbasis alkohol. Mencuci tangan sampai bersih selain dapat membunuh virus yang mungkin ada di tangan kita, tindakan ini juga merupakan salah satu tindakan yang mudah dan murah. Sekitar 98% penyebaran penyakit bersumber dari tangan. Karena itu, menjaga kebersihan tangan adalah hal yang sangat penting." +
                            "\n\n3. Ketika batuk dan bersin, upayakan menjaga agar lingkungan Anda tidak tertular. Tutup hidung dan mulut Anda dengan tisu atau dengan lengan (bukan dengan telapak tangan)." +
                            "\n\n4. Menjaga jarak saat berbicara dengan orang lain, sekurang- kurangnya satu meter, terutama dengan orang yang sedang menderita batuk, pilek/bersin dan demam. Saat seseorang terinfeksi penyakit saluran pernafasan, seperti 2019-nCoV, batuk/bersin dapat menghasilkan droplet yang mengandung virus. Jika kita terlalu dekat, virus tersebut dapat terhirup oleh kita." +
                            "\n\n5. Hindari menyentuh mata, hidung dan mulut. Tangan menyentuh banyak hal yang dapat terkontaminasi virus. Jika kita menyentuh mata, hidung dan mulut dengan tangan yang terkontaminasi, maka virus dapat dengan mudah masuk ke tubuh kita." +
                            "\n\n6. Gunakan masker penutup mulut dan hidung ketika Anda sakit atau saat berada di tempat umum." +
                            "\n\n7. Buang tisu dan masker yang sudah digunakan ke tempat sampah, lalu cucilah tangan Anda." +
                            "\n\n8. Hindari kontak dengan hewan ternak dan hewan liar yang terbukti tertular coronavirus." +
                            "\n\n9. Jangan makan daging yang tidak dimasak hingga matang." +
                            "\n\n10. Menunda perjalanan ke daerah/negara dimana virus ini ditemukan seperti Tiongkok, seiring dengan informasi adanya penghentian sementara operasional penerbangan langsung dari dan ke daratan Cina dari pemerintah, sampai ada informasi lebih lanjut."
            );
        }});

        //3
        listDataChildTemp.add(new ArrayList<String>() {{
            add(
                    "Gejala umum berupa demam ≥38C, batuk, pilek, nyeri tenggorokan dan sesak napas. Jika ada orang dengan gejala tersebut pernah melakukan perjalanan ke Tiongkok (terutama Wuhan), atau pernah merawat/kontak dengan penderita COVID-19, maka terhadap orang tersebut akan dilakukan pemeriksaan laboratorium lebih lanjut untuk memastikan diagnosisnya."
            );
        }});

        //4
        listDataChildTemp.add(new ArrayList<String>() {{
            add(
                    "Seperti penyakit pernapasan lainnya, infeksi 2019-nCoV dapat menyebabkan gejala ringan termasuk pilek, sakit tenggorokan, batuk, dan demam. Beberapa orang mungkin akan menderita sakit yang parah, seperti disertai pneumonia atau kesulitan bernafas. Walaupun fatalitas penyakit ini masih jarang, namun bagi orang yang berusia lanjut, dan orang-orang dengan kondisi medis yang sudah ada sebelumnya (seperti, diabetes dan penyakit jantung), mereka biasanya lebih rentan untuk menjadi sakit parah."
            );
        }});

        //5
        listDataChildTemp.add(new ArrayList<String>() {{
            add(
                    "Sampai saat ini, belum diketahui bagaimana manusia bisa terinfeksi virus ini. Para ahli masih sedang melakukan penyelidikan untuk menentukan sumber virus, jenis paparan, cara penularan dan pola klinis serta perjalanan penyakit. Hasil penyelidikan sementara dari beberapa institusi di kota Wuhan, sebagian kasus terjadi pada orang yang bekerja di pasar hewan/ikan, namun belum dapat dipastikan jenis hewan penular virus ini. Hingga saat ini dilaporkan adanya penularan antar manusia yang terbatas (antar keluarga dekat dan petugas kesehatan yang merawat kasus)."
            );
        }});

        //6
        listDataChildTemp.add(new ArrayList<String>() {{
            add(
                    "Saat ini sumber hewan penular COVID-19 belum diketahui, WHO terus menyelidiki berbagai kemungkinan jenis hewan penularnya. Sangat dimungkinkan hewan dari pasar hewan hidup di Tiongkok bertanggung jawab atas terinfeksinya manusia yang dilaporkan pertama kali. Untuk itu, disarankan pada saat berkunjung ke pasar hewan  hidup, hindari kontak langsung dengan hewan dan permukaan yang bersentuhan dengan hewan tanpa alat pelindung diri. \n" +
                            "\n" +
                            "Hindari juga konsumsi produk hewani mentah atau setengah matang. Penanganan daging mentah, susu, atau produk hewani harus diperhatikan, untuk menghindari kontaminasi silang dengan makanan mentah yang lain, lakukanlah dengan memperhatikan keamanan pangan yang baik."
            );
        }});

        //7
        listDataChildTemp.add(new ArrayList<String>() {{
            add(
                    "Sampai saat ini hewan penular 2019-nCoV belum diketahui."
            );
        }});

        //8
        listDataChildTemp.add(new ArrayList<String>() {{
            add(
                    "Ya, COVID-19 dapat ditularkan dari orang ke orang, biasanya setelah kontak erat dengan pasien yang terinfeksi, misalnya, di tempat kerja, di rumah tangga, atau fasilitas pelayanan kesehatan."
            );
        }});

        //9
        listDataChildTemp.add(new ArrayList<String>() {{
            add(
                    "Sampai saat ini belum diketahui berapa lama 2019-nCoV bertahan di permukaan suatu benda, meskipun ada informasi awal yang menunjukkan dapat bertahan hingga beberapa jam. Namun desinfektan sederhana dapat membunuh virus tersebut sehingga tidak mungkin menginfeksi orang lagi."
            );
        }});

        //10
        listDataChildTemp.add(new ArrayList<String>() {{
            add(
                    "Belum ada vaksin atau pengobatan spesifik untuk virus ini. Namun, gejala yang disebabkan oleh virus ini dapat diobati. Oleh karena itu pengobatan harus didasarkan pada kondisi klinis pasien dan perawatan suportif dapat sangat efektif."
            );
        }});

        //11
        listDataChildTemp.add(new ArrayList<String>() {{
            add(
                    "Tidak, antibiotik tidak bekerja melawan virus, hanya bakteri. Novel Coronavirus (2019-nCoV) adalah virus. Oleh karena itu, antibiotik tidak boleh digunakan sebagai sarana pencegahan atau pengobatan. Namun, jika Anda dirawat di rumah sakit untuk COVID-19, Anda mungkin menerima antibiotik, karena infeksi sekunder bakteri mungkin terjadi."
            );
        }});

        //12
        listDataChildTemp.add(new ArrayList<String>() {{
            add(
                    "Orang yang tinggal atau bepergian di daerah di mana virus 2019-nCoV bersirkulasi sangat mungkin berisiko terinfeksi. Saat ini, Tiongkok merupakan negara terjangkit COVID-19 dengan sebagian besar kasus telah dilaporkan. Mereka yang terinfeksi di negara lain adalah orang-orang yang belum lama ini bepergian dari Tiongkok atau yang telah tinggal atau bekerja secara dekat dengan para wisatawan, seperti anggota keluarga, rekan kerja atau tenaga medis yang merawat pasien sebelum mereka tahu pasien tersebut terinfeksi COVID-19.\n" +
                            "\n" +
                            "Petugas kesehatan yang merawat pasien yang terinfeksi COVID-19 berisiko lebih tinggi dan harus konsisten melindungi diri mereka sendiri dengan prosedur pencegahan dan pengendalian infeksi yang tepat."
            );
        }});

        //13
        listDataChildTemp.add(new ArrayList<String>() {{
            add(
                    "Tidak ada batasan usia orang-orang dapat terinfeksi COVID-19. Namun orang yang lebih tua dan orang-orang dengan kondisi medis yang sudah ada sebelumnya (seperti asma, diabetes, penyakit jantung) tampaknya lebih rentan untuk menderita sakit parah."
            );
        }});

        //14
        listDataChildTemp.add(new ArrayList<String>() {{
            add(
                    "Orang yang terinfeksi 2019-nCoV dan influenza akan mengalami gejala infeksi saluran pernafasan yang sama, seperti demam, batuk dan pilek. Walaupun gejalanya sama, tapi penyebab virusnya berbeda-beda. Namun kesamaan gejala tersebut membuat kita sulit mengidentifikasi masing-masing penyakit tersebut, sehingga pemeriksaan laboratorium sangat diperlukan untuk mengkonfirmasi apakah seseorang terinfeksi 2019-nCoV.\n" +
                        "\n" +
                        "Untuk itulah WHO merekomendasikan setiap orang yang menderita demam, batuk, dan sulit bernapas harus mencari pengobatan sejak dini, dan memberitahukan petugas kesehatan jika mereka telah melakukan perjalanan dalam 14 hari sebelum muncul gejala, atau jika mereka telah melakukan kontak erat dengan seseorang yang sedang menderita gejala infeksi saluran pernafasan."
            );
        }});

        //15
        listDataChildTemp.add(new ArrayList<String>() {{
            add(
                    "Waktu yang diperlukan sejak tertular/terinfeksi hingga muncul gejala disebut masa inkubasi. Saat ini masa inkubasi COVID-19 diperkirakan antara 2-11 hari, dan perkiraan ini dapat berubah sewaktu-waktu sesuai perkembangan kasus. Berdasarkan data dari penyakit akibat coronavirus sebelumnya, seperti MERS dan SARS, masa inkubasi COVID-19 juga bisa mencapai 14 hari."
            );
        }});

        for (int i = 0; i<this.listDataHeader.size(); i++) {
            this.listDataChild.put(listDataHeader.get(i), listDataChildTemp.get(i));
        }
    }

    public List<String> getDataHeader() {
        return this.listDataHeader;
    }

    public HashMap<String, List<String>> getDataChild() {
        return this.listDataChild;
    }
}
