package com.pppb.covid19project.view;

import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.pppb.covid19project.CovidLiveData;
import com.pppb.covid19project.databinding.DataFragmentBinding;
import com.pppb.covid19project.model.Country;
import com.pppb.covid19project.model.DayOne;
import com.pppb.covid19project.model.Summary;

import java.util.ArrayList;
import java.util.List;

public class DataFragment extends Fragment {
    private DataFragmentBinding binding;
    private CovidLiveData model;
    private Spinner spinner;
    protected int alreadyStarted = 0;
    private Handler chartHandler = new Handler();

    public DataFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        this.binding = DataFragmentBinding.inflate(inflater, container, false);
        View view = binding.getRoot();

        final LoadingDialog loadingDialog = new LoadingDialog(this.getActivity());
        if (alreadyStarted == 0) {
            loadingDialog.startLoadingDialog();
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    loadingDialog.dismissDialog();
                }
            }, 3000);
            alreadyStarted++;
            Log.d("debug", "AlreadyStarted: " + alreadyStarted);
        }

        this.spinner = binding.spinner;
        List<Country> countryModelList = new ArrayList<>();

        model = new ViewModelProvider(requireActivity()).get(CovidLiveData.class);

        ArrayAdapter<Country> adapter = new ArrayAdapter<Country>(this.getContext(), android.R.layout.simple_spinner_item, countryModelList);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner.setAdapter(adapter);
        model.getSummary().observe(this, new Observer<Summary>() {
            @Override
            public void onChanged(Summary summary) {
                binding.tvConfirmedResultGlobal.setText(convertNumberToString(summary.getGlobal().getTotalConfirmed()));
                binding.tvDeceasedResultGlobal.setText(convertNumberToString(summary.getGlobal().getTotalDeaths()));
                binding.tvActiveResultGlobal.setText(convertNumberToString(summary.getGlobal().getTotalActive()));
                binding.tvRecoveredResultGlobal.setText(convertNumberToString(summary.getGlobal().getTotalRecovered()));

                binding.tvNewConfirmedGlobal.setText("+" + convertNumberToString(summary.getGlobal().getNewConfirmed()));
                binding.tvNewDeceasedGlobal.setText("+" + convertNumberToString(summary.getGlobal().getNewDeaths()));
                binding.tvNewRecoveredGlobal.setText("+" + convertNumberToString(summary.getGlobal().getNewRecovered()));
                if (summary.getGlobal().getNewActive() < 0) {
                    binding.tvNewActiveGlobal.setText(convertNumberToString(summary.getGlobal().getNewActive()));
                } else {
                    binding.tvNewActiveGlobal.setText("+" + convertNumberToString(summary.getGlobal().getNewActive()));
                }

                binding.lastUpdatedGlobal.setText("Last Updated : " + summary.getDate());

                countryModelList.clear();
                countryModelList.addAll(summary.getCountries());
                adapter.notifyDataSetChanged();
            }
        });


        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Country country = (Country) parent.getSelectedItem();
                displayDataCountry(country);
                binding.tvConfirmedResultLocal.setText(convertNumberToString(country.getTotalConfirmed()));
                binding.tvRecoveredResultLocal.setText(convertNumberToString(country.getTotalRecovered()));
                binding.tvDeceasedResultLocal.setText(convertNumberToString(country.getTotalDeaths()));
                binding.tvActiveResultLocal.setText(convertNumberToString(country.getTotalActive()));

                binding.tvNewConfirmedLocal.setText("+" + convertNumberToString(country.getNewConfirmed()));
                binding.tvNewRecoveredLocal.setText("+" + convertNumberToString(country.getNewRecovered()));
                binding.tvNewDeceasedLocal.setText("+" + convertNumberToString(country.getNewDeaths()));
                if (country.getNewActive() < 0) {
                    binding.tvNewActiveLocal.setText(convertNumberToString(country.getNewActive()));
                } else {
                    binding.tvNewActiveLocal.setText("+" + convertNumberToString(country.getNewActive()));
                }

                binding.lastUpdated.setText("Last Updated : " + country.getDate());

                String currentSlug = country.getSlug();
                Log.d("debug", "onItemSelected: " + currentSlug);

                ArrayList<Entry> confirmed = new ArrayList<Entry>();
                ArrayList<Entry> recovered = new ArrayList<Entry>();
                ArrayList<Entry> active = new ArrayList<Entry>();
                ArrayList<Entry> death = new ArrayList<Entry>();

                if(!currentSlug.equalsIgnoreCase("united-states")){
                    if (alreadyStarted == 0) {
                        model.setCurrentCountry(currentSlug);
                    }
                    model.loadDayOne(currentSlug);
                    model.getNewDayOne().observe(getViewLifecycleOwner(), new Observer<List<DayOne>>() {
                        @Override
                        public void onChanged(List<DayOne> dayOnes) {
                            List<DayOne> topHundredList = new ArrayList<>();
                            if (dayOnes.size() < 100) {
                                topHundredList.addAll(dayOnes);
                            } else {
                                topHundredList = dayOnes.subList(dayOnes.size()-100, dayOnes.size());
                            }
                            List<DayOne> finalTopHundredList = topHundredList;
                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    confirmed.clear();
                                    recovered.clear();
                                    active.clear();
                                    death.clear();
                                    for (int i = 0; i < finalTopHundredList.size(); i++) {
                                        confirmed.add(new Entry((float) i, (float) finalTopHundredList.get(i).getConfirmed()));
                                        recovered.add(new Entry((float) i, (float) finalTopHundredList.get(i).getRecovered()));
                                        active.add(new Entry((float) i, (float) finalTopHundredList.get(i).getActive()));
                                        death.add(new Entry((float) i, (float) finalTopHundredList.get(i).getDeaths()));
                                    }

                                    LineDataSet confirmDataSet = new LineDataSet(confirmed, "Confirmed");
                                    confirmDataSet.setMode(LineDataSet.Mode.CUBIC_BEZIER);
                                    confirmDataSet.setCircleColor(Color.GREEN);
                                    confirmDataSet.setCircleRadius(1f);
                                    confirmDataSet.setColor(Color.GREEN);

                                    LineDataSet recoverDataSet = new LineDataSet(recovered, "Recovered");
                                    recoverDataSet.setMode(LineDataSet.Mode.CUBIC_BEZIER);
                                    recoverDataSet.setCircleColor(Color.BLUE);
                                    recoverDataSet.setCircleRadius(1f);
                                    recoverDataSet.setColor(Color.BLUE);

                                    LineDataSet deathDataSet = new LineDataSet(death, "Death");
                                    deathDataSet.setMode(LineDataSet.Mode.CUBIC_BEZIER);
                                    deathDataSet.setCircleColor(Color.RED);
                                    deathDataSet.setCircleRadius(1f);
                                    deathDataSet.setColor(Color.RED);

                                    LineDataSet activeDataSet = new LineDataSet(active, "Active");
                                    activeDataSet.setMode(LineDataSet.Mode.CUBIC_BEZIER);
                                    activeDataSet.setCircleColor(Color.YELLOW);
                                    activeDataSet.setCircleRadius(1f);
                                    activeDataSet.setColor(Color.YELLOW);

                                    Legend legend = binding.lineChart.getLegend();
                                    legend.setEnabled(true);
                                    legend.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
                                    legend.setHorizontalAlignment(Legend.LegendHorizontalAlignment.CENTER);
                                    legend.setOrientation(Legend.LegendOrientation.HORIZONTAL);
                                    legend.setDrawInside(false);

                                    chartHandler.post(new Runnable() {
                                        @Override
                                        public void run() {
                                            binding.lineChart.getDescription().setEnabled(false);
                                            binding.lineChart.getXAxis().setDrawLabels(false);
                                            binding.lineChart.getAxisRight().setDrawLabels(false);
                                            binding.lineChart.setData(new LineData(confirmDataSet, recoverDataSet, activeDataSet, deathDataSet));
                                            binding.lineChart.animateXY(0, 0);
                                        }
                                    });
                                }
                            }).start();
                        }
                    });
                } else {
                    confirmed.clear();
                    recovered.clear();
                    active.clear();
                    death.clear();
                    binding.lineChart.getData().clearValues();
                    binding.lineChart.invalidate();
                    binding.lineChart.clear();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        binding.lineChart.setTouchEnabled(false);


        return view;
    }

    private void displayDataCountry(Country countryModel) {
        String slug = countryModel.getCountry();
        String show = "You Selected : " + slug;
        Toast.makeText(this.getContext(), show, Toast.LENGTH_SHORT).show();
    }

    private String convertNumberToString(int num) {
        String result = "";
        String tempString = "";

        int temp = 0;
        while(num/1000 >= 1) {
            temp = num%1000;
            if(temp < 10) {
                tempString = ("00" + Integer.toString(temp));
            } else if (temp < 100) {
                tempString = ("0" + Integer.toString(temp));
            } else {
                tempString = Integer.toString(temp);
            }

            result = "." + tempString + result;
            num /= 1000;
        }
        result = Integer.toString(num) + result;
        return result;
    }

}
