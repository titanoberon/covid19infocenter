package com.pppb.covid19project.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListAdapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.pppb.covid19project.databinding.FaqFragmentBinding;

public class FAQFragment extends Fragment {
    private FaqFragmentBinding binding;
    private ExpandableListAdapter expandableListAdapter;

    public FAQFragment() {
    }

    public static FAQFragment newInstance(ExpandableListAdapter expandableListAdapter){
        FAQFragment ff = new FAQFragment();
        ff.expandableListAdapter = expandableListAdapter;
        return ff;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        this.binding = FaqFragmentBinding.inflate(inflater, container, false);
        View view = binding.getRoot();

        this.binding.expandableListView.setAdapter(expandableListAdapter);

        return view;
    }
}
