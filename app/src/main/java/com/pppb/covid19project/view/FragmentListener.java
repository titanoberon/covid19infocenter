package com.pppb.covid19project.view;

public interface FragmentListener {
    void changePage(String page);
    void restartApp();
}
