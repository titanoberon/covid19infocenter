package com.pppb.covid19project.view;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.pppb.covid19project.R;
import com.pppb.covid19project.databinding.HomeFragmentBinding;
import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ImageListener;

public class HomeFragment extends Fragment implements View.OnClickListener {
    private HomeFragmentBinding binding;
    private Button callGreen;
    private Button callRed;
    private Button webButton;
    private FragmentListener listener;
    CarouselView carouselView;
    int[] images = {R.drawable.image_1, R.drawable.image_2, R.drawable.image_3, R.drawable.image_4};

    public HomeFragment() {

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        this.binding = HomeFragmentBinding.inflate(inflater, container, false);
        View view = binding.getRoot();

        this.callGreen = binding.callGreen;
        this.callGreen.setOnClickListener(this);
        this.callRed = binding.callRed;
        this.callRed.setOnClickListener(this);
        this.webButton = binding.button1;
        this.webButton.setOnClickListener(this);
        this.carouselView = binding.carouselView;
        this.carouselView.setPageCount(images.length);
        this.carouselView.setImageListener(imageListener);

        return view;
    }

    ImageListener imageListener = new ImageListener() {
        @Override
        public void setImageForPosition(int position, ImageView imageView) {
            Glide.with(getContext()).load(images[position]).into(imageView);
        }
    };

    @Override
    public void onClick(View v) {
        if(v == this.webButton) {
            this.listener.changePage("webview");
        }else if(v == this.callGreen) {
            Intent intent = new Intent(Intent.ACTION_DIAL);
            intent.setData(Uri.parse("tel:119"));
            startActivity(intent);
        }else if(v == this.callRed) {
            Intent intent = new Intent(Intent.ACTION_DIAL);
            intent.setData(Uri.parse("tel:081212123119"));
            startActivity(intent);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof FragmentListener){
            this.listener = (FragmentListener) context;
        }
        else{
            throw new ClassCastException(context.toString() + " must implement FragmentListener");
        }
    }
}
