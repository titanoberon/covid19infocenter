package com.pppb.covid19project.view;

public interface IOnBackPressed {
    boolean onBackPressed();
}
