package com.pppb.covid19project.view;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkCapabilities;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.pppb.covid19project.MainActivity;
import com.pppb.covid19project.R;
import com.pppb.covid19project.databinding.SettingsFragmentBinding;

public class SettingsFragment extends Fragment  {
    private SettingsFragmentBinding binding;
    private NetworkCapabilities networkCapabilities;
    private boolean mIsNightMode;
    private static SharedPreferences sp;
    private FragmentListener listener;

    public SettingsFragment() {
    }

    public static SettingsFragment newInstance(SharedPreferences sharedPreferences) {
        SettingsFragment settingsFragment = new SettingsFragment();
        settingsFragment.sp = sharedPreferences;
        return settingsFragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        this.binding = SettingsFragmentBinding.inflate(inflater, container, false);
        View view = binding.getRoot();



        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES){
            binding.switchDarkMode.setChecked(true);
        } else {
            binding.switchDarkMode.setChecked(false);
        }

        binding.switchDarkMode.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    saveTheme(true);
                } else {
                    saveTheme(false);
                }
            }
        });


        ConnectivityManager connectivityManager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        this.networkCapabilities = connectivityManager.getNetworkCapabilities(connectivityManager.getActiveNetwork());

        if (networkCapabilities != null){
            binding.tvNetworkStatus.setText("Connected");
            binding.tvNetworkStatus.setTextColor(ContextCompat.getColor(getContext(), R.color.call_green));
        } else {
            binding.tvNetworkStatus.setText("Not Connected");
            binding.tvNetworkStatus.setTextColor(ContextCompat.getColor(getContext(), R.color.call_red));
        }

        return view;
    }


    public void saveTheme(boolean state) {
        SharedPreferences.Editor editor = sp.edit();
        editor.putBoolean(MainActivity.KEY_THEME, state);
        editor.apply();
        this.listener.restartApp();
    }

    public void onAttach(Context context){
        super.onAttach(context);
        if(context instanceof FragmentListener){
            this.listener = (FragmentListener) context;
        }
        else{
            throw new ClassCastException(context.toString() + " must implement FragmentListener");
        }
    }
}
