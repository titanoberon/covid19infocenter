package com.pppb.covid19project.view;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.pppb.covid19project.databinding.WebviewFragmentBinding;

public class WebViewFragment extends Fragment implements IOnBackPressed {
    private WebviewFragmentBinding binding;
    private WebView webView;
    private FragmentListener listener;

    public WebViewFragment() {

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        this.binding = WebviewFragmentBinding.inflate(inflater, container, false);
        View view = binding.getRoot();

        this.webView = binding.webview;
        this.webView.setWebViewClient(new WebViewClient());
        this.webView.loadUrl("https://covid19.who.int/");

        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);

        return view;
    }


    @Override
    public boolean onBackPressed() {
        if (webView.canGoBack()) {
            Log.d("backPressed", "canGoBack" + webView.canGoBack());
            webView.goBack();
        } else {
            Log.d("backPress", "back pressed");
            this.listener.changePage("home");
        }
        return true;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof FragmentListener){
            this.listener = (FragmentListener) context;
        }
        else{
            throw new ClassCastException(context.toString() + " must implement FragmentListener");
        }
    }
}
